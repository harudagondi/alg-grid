# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2017-06-20

### Added

- struct for points in two and three dimensions.
- trait for implementing a grid in two and three dimensions.
- implemented Dijkstra's and A* search algorithms for two and three dimensions.
