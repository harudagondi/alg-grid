# alg-grid

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline/harudagondi/alg-grid?style=flat-square)](https://gitlab.com/harudagondi/alg-grid/commits/master)

`alg-grid` is a collection of helper functions for 2D and 3D grids, including Dijkstra's and A* algorithm.

These functions are implemented using this [website](https://www.redblobgames.com/pathfinding/a-star/implementation.html) as a reference. Other functions came from the `bracket_lib` crate.

This crate does not require a standard library.

## Table of Contents

- [alg-grid](#alg-grid)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

Put this in your `Cargo.toml` file.

```toml
[dependencies]
alg-grid = "0.1"
```

## Usage

// TODO: add examples.

```rust
```

## Maintainers

[@harudagondi](https://github.com/harudagondi)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MPL-2.0 © Gio Genre De Asis
