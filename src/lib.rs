// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! alg-grid
//! 
//! `alg-grid` is a collection of helper functions for
//! 2D and 3D grids, including Dijkstra's and A* algorithm.
//! 
//! These functions are implemented using this [website](https://www.redblobgames.com/pathfinding/a-star/implementation.html)
//! as a reference. Other functions came from the `bracket_lib` crate.
//! 
//! This crate does not require a standard library.

#![no_std]

pub mod two_dim;
pub mod three_dim;

pub mod prelude {
    #[cfg(feature = "two_dim")]
    pub use crate::two_dim::*;
    #[cfg(feature = "three_dim")]
    pub use crate::three_dim::*;
    pub use num_rational::BigRational;
}

pub use prelude::*;